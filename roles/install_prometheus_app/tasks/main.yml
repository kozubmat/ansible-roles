---
- name: Add copy target dir
  ansible.builtin.file:
    path: "{{copy_path}}"
    state: directory

- name: Copy Prometheus files
  copy:
    src: "{{role_path}}/files/{{prometheus_file}}"
    dest: "{{copy_path}}"
    owner: prometheus
    group: prometheus
    backup: yes

- name: Add unzip target dir "{{prometheus_home_dir}}/{{prometheus_untar_dir}}"
  ansible.builtin.file:
    path: "{{prometheus_home_dir}}/{{prometheus_untar_dir}}"
    state: directory
    owner: prometheus
    group: prometheus

- name: Unarchive prometheus files
  unarchive:
    src: "{{copy_path}}/{{prometheus_file}}"
    dest: "{{prometheus_home_dir}}"
    owner: prometheus
    group: prometheus
    remote_src: yes

- name: Create symlinks for service
  file:
    src: "{{prometheus_home_dir}}/{{prometheus_untar_dir}}"
    dest: "{{prometheus_home_dir}}/latest"
    state: link
    owner: prometheus
    group: prometheus
    force: yes

- name: Template a Prometheus service file to /etc/systemd/system/prometheus.service
  ansible.builtin.template:
    src: "{{role_path}}/templates/prometheus.service.j2"
    dest: /etc/systemd/system/prometheus.service
    owner: root
    group: root
    mode: '0644'

- name: Template a Prometheus timer file to /etc/systemd/service
  ansible.builtin.template:
    src: "{{role_path}}/templates/prometheus.timer.j2"
    dest: /etc/systemd/system/prometheus.timer
    owner: root
    group: root
    mode: '0644'

- name: Add config dir {{prometheus_config_dir}}
  ansible.builtin.file:
    path: "{{prometheus_config_dir}}"
    state: directory
    owner: prometheus
    group: prometheus

- name: Add prometheus DB dir {{prometheus_db_dir}}
  ansible.builtin.file:
    path: "{{prometheus_db_dir}}"
    owner: prometheus
    group: prometheus
    state: directory

- name: Template a prometheus config file to {{prometheus_config_dir}}
  ansible.builtin.template:
    src: "{{role_path}}/templates/prometheus.yml.j2"
    dest: "{{prometheus_config_dir}}/prometheus.yml"
    owner: prometheus
    group: prometheus
    mode: '0644'

- name: Add RO config dir for TLS config {{prometheus_read_only_dirs}}
  ansible.builtin.file:
    path: "{{prometheus_read_only_dirs}}"
    state: directory
    owner: prometheus
    group: prometheus
  when: '{{var_prometheus_tls}} != 0'

- name: Template a prometheus TLS config file to {{prometheus_config_dir}}
  ansible.builtin.template:
    src: "{{role_path}}/templates/prometheus_tls.yml.j2"
    dest: "{{prometheus_config_dir}}/web.yml"
    owner: prometheus
    group: prometheus
    mode: '0644'
  when: '{{var_prometheus_tls}} != 0'

- name: Template a query logrotate config file 
  ansible.builtin.template:
    src: "{{role_path}}/templates/logrotate_query_prometheus.j2"
    dest: /etc/logrotate.d/prometheus
    owner: root
    group: root
    mode: '0644'

- name: "Rediscover systemd services"
  include_role:
    name: rediscover_systemd_service

- name: Make sure Prometheus service unit is running
  ansible.builtin.systemd:
    enabled: yes
    state: started
    name: "{{item}}"
  with_items: 
      - prometheus.service
      - prometheus.timer